An Introduction to Extender
===========================

Add an OE Order Line
--------------------

This package contains the content used to deliver an introduction
to `Orchid Extender`_ to a group of developers.  The training was developed
and delivered by `2665093 Ontario Inc`_.

.. _Orchid Extender: https://www.orchid.systems/product/extender
.. _2665093 Ontario Inc: https://2665093.ca

This package contains two extender scripts, both of which do the same thing
but are implemented in different ways: one as a view script and one as a 
screen script. The training material also discusses using Extender module
files to group and version a customization as well as how to add custom
tables.

The rendered documentation at https://poplar-oeaddlne.readthedocs.io 
is probably what you want.  This content is distributed under the 
Creative Commons Share Alike 4 license, feel free to modify, share,
and reuse it.
