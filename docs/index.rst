.. poplay_oeaddlne documentation master file, created by
   sphinx-quickstart on Tue Dec 17 15:19:25 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Extender Introductory Training: Add an OE Order Line
====================================================

.. toctree::
   :hidden:
   :maxdepth: 4

   src/walkthrough
   src/OE0500_oe_add_line
   src/OE1100_oe_add_line
   src/resources

This package contains the content for a quick introductory training
session to `Orchid Extender`_.  The training was delivered to a team
endeavouring to build their own scripts to manage OE Orders.

.. _Orchid Extender: https://www.orchid.systems/product/extender

The content was developed and delivered by `2665093 Ontario Inc`_. If
you'd like to receive custom Orchid Extender training or hands on help
solving a problem using Extender, `send us an email`_.

.. _2665093 Ontario Inc: https://2665093.ca
.. _send us an email: contact@2665093.ca

Download the `git repository`_ and follow along with the `presentation`_.

Once you've reached the ``Build It`` section, continue to 
:doc:`src/walkthrough/building_the_view_script`.


.. _git repository: https://bitbucket.org/cbinckly/poplar_oeaddlne/
.. _presentation: https://docs.google.com/presentation/d/13iTLpDOrrTWY5JIkQ5f-HfxFSBrjBUeYULsXtOrwo80/
