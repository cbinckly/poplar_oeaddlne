Resources
=========

.. toctree::
    :hidden:
    :maxdepth: 4

    resources/all_view_calls

Some references to get started with:

- The presentation: `Extender Introduction`_.
- `Sample scripts`_ from Orchid.
- `Sample scripts`__ from 2665093
- `Orchid Knowledgebase`_

.. _Extender Introduction: https://docs.google.com/presentation/d/13iTLpDOrrTWY5JIkQ5f-HfxFSBrjBUeYULsXtOrwo80
.. _Sample scripts: https://www.orchid.systems/resources/scripts
.. __: https://2665093.ca
.. _Orchid Knowledgebase: https://orchidsystems.zendesk.com/

There are `sample customizations`_ for the ``OE1100`` screen from Orchid in 
``poplar_oeaddlne.resources.examples`` as well as a searchable, 
annotated version of :doc:`resources/all_view_calls`.

.. _sample customizations: https://bitbucket.org/cbinckly/poplar_oeaddlne/src/master/poplar_oeaddlne/resources/examples/
