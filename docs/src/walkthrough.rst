==========================
Walkthrough
==========================

.. toctree::
   :hidden:
   :maxdepth: 4

   walkthrough/building_the_view_script
   walkthrough/building_the_screen_script
   walkthrough/making_the_module
   walkthrough/adding_custom_tables

It is time to build the code. These narrated walkthroughs cover
the development process for :py:mod:`poplar_oeaddlne.OE0500_oe_add_line`
and :py:mod:`poplar_oeaddlne.OE1100_oe_add_line` Python scripts, putting
the final script into an Extender module, and upgrading the module and
script to lookup the item information from a custom table.
